import React, { Component } from 'react';

export default class ClockClass extends Component {
  constructor() {
    super();
    this.state = {
      date: new Date(),
    };
  }
  tick() {
    this.setState({
      date: new Date(),
    });
  }

  componentDidMount() {
    this.timerID = setInterval(() => {
      this.tick();
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  render() {
    return (
      <div className="flex justify-center flex-col items-center h-screen">
        <div className="font-bold border-b-2 border-black w-56 text-center text-2xl">
          CLOCK class
        </div>
        <div className="font-2xl text-black">
          {this.state.date.toLocaleTimeString()}
        </div>
      </div>
    );
  }
}
