import { cleanup } from '@testing-library/react';
import React, { useEffect, useState } from 'react';

export default function Clock() {
  const [showDate, setShowDate] = useState(new Date());

  useEffect(() => {
    let timerID = setInterval(() => {
      tick();
    }, 1000);
    return function cleanup() {
      clearInterval(timerID);
    };
  });
  const tick = () => {
    setShowDate(new Date());
  };
  return (
    <div className="flex justify-center flex-col items-center h-screen">
      <div className="font-bold border-b-2 border-black w-56 text-center text-2xl">
        CLOCK hooks
      </div>
      <div className="font-2xl text-black">{showDate.toLocaleTimeString()}</div>
    </div>
  );
}
