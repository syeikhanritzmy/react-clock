import Clock from './components/Clock';
import ClockClass from './components/ClockClass';

function App() {
  return (
    <div className="App">
      <div className="flex justify-center gap-x-4">

        <Clock />
        <ClockClass />
      </div>
    </div>
  );
}

export default App;
